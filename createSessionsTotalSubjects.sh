#/bin/sh

####################################################
## createSessionsTotalSubjects.sh
## script for creating a skeleton project structure (PET sesions without files)
## This is used for testing some web functionality where large numbers of sessions are there, but files aren't required.
##
## Author: Tim Olsen <tim@radiologics.com>
## Last Modified: 9/25/15
####################################################

HOST=$1
CRED=$2
STUDIES=$3
RAND_SITES=8
RAND_SUBJECTS=10000
TOTAL_SUBJECTS=$4
RUN=$5

##original arguments 3 10 10

TRACER=1
YEAR=1930
GROUP=1
GENDER=0

if [ "$#" -ne 5 ]; then 
	echo "Inproper parameters HOST CRED STUDIES TOTAL_SUBJECTS RUN"
	exit 1 
fi

## first create a reusable JSESSIONID so we don't have to reauthenticate on every request.  This is optional but more efficient.
curl -k -X POST -u "$CRED" "$HOST/data/JSESSION" -i > ~/JSESSION.txt
JSESSION=`grep "JSESSIONID" ~/JSESSION.txt`
JSESSION=${JSESSION:23:32}
echo "JSESSION created: $JSESSION"

function createSubjects {
	STUD="STUDY${RUN}${study}"
	for((i=0;i<$SITES;i++))
	do
		SITE="SITE_${RUN}$study"
		SUBJECTS=$(( (( RANDOM % $RAND_SUBJECTS ) + 1 ) * (2)))
		PROJECT=$SITE
		PROJECT+="_"
		PROJECT+=$i
    		echo "$PROJECT in $STUD with $SUBJECTS subjects"
		curl  -b "JSESSIONID=$JSESSION" -X PUT $HOST/REST/projects/$PROJECT?req_format=form -d xnat:projectData/fields/field[name%3Dstudy_id]/field=$STUD

		for((sC=0;sC<$SUBJECTS;sC++))
		do
			SUBJECT="S$study"
			SUBJECT+="_"
			SUBJECT+=$i
			SUBJECT+="_"
			SUBJECT+=$sC
			
			if [ $TRACER -eq 4 ]
			then
				TRACER=1
			else
				TRACER=$((TRACER+1))
			fi

			if [ $GENDER -eq 2 ]
			then
				GENDER=0
				GENDERTEXT=male
			else
				GENDER=$((GENDER+1))
				GENDERTEXT=female
			fi

			if [ $YEAR -eq 1987 ]
			then
				YEAR=1940
			else
				YEAR=$((YEAR+1))
			fi

			if [ $GROUP -eq 3 ]
			then
				GROUP=1
			else
				GROUP=$((GROUP+1))
			fi


		 START=$(date +%s.%N)
		 echo "${RUN} Create subject"
		 curl --silent -w ',%{http_code},%{time_total},%{time_connect},%{time_appconnect},%{time_pretransfer},%{time_starttransfer}\n' -b "JSESSIONID=$JSESSION" -X PUT "$HOST/REST/projects/$PROJECT/subjects/$SUBJECT?event_reason=test&req_format=form&gender=$GENDERTEXT&dob=02/14/$YEAR" -d xnat:subjectData/group=group$GROUP >> $SUBDIR/subjects-curl.csv
		 END=$(date +%s.%N)
		 DIFF=$(echo "$END - $START" | bc)
		 echo ".... ${DIFF}"
		echo "${SUBJ_COUNTER},${DIFF}" >> $SUBDIR/subjects.csv

		 START=$(date +%s.%N)
		 echo "${RUN} Create PET session"
		 curl --silent -w ',%{http_code},%{time_total},%{time_connect},%{time_appconnect},%{time_pretransfer},%{time_starttransfer}\n'  -b "JSESSIONID=$JSESSION" -X PUT "$HOST/REST/projects/$PROJECT/subjects/$SUBJECT/experiments/PET_$SUBJECT?event_reason=test&req_format=form&xsiType=xnat:petSessionData" -d "xnat:petSessionData/date=01/11/2013" -d "xnat:petSessionData/visit_id=v1" -d "xnat:petSessionData/tracer/name=TRACER$TRACER" >> $SUBDIR/sessions-curl.csv
		 END=$(date +%s.%N)
		 DIFF=$(echo "$END - $START" | bc)
		 echo ".... ${DIFF}"
		
		 echo "${SUBJ_COUNTER},${DIFF}" >> $SUBDIR/sessions.csv
		 let SUBJ_COUNTER=SUBJ_COUNTER+1
		 
		 if [ $SUBJ_COUNTER -gt $TOTAL_SUBJECTS ]
		 then
			return 0
		 fi
		done
	 let SITE_COUNTER=SITE_COUNTER+1
	done
}

SUBDIR=$(date +%s)
mkdir $SUBDIR
echo "subject,total,connect,appconnect,pretransfer,starttransfer" > $SUBDIR/subjects.csv
echo "session,total,connect,appconnect,pretransfer,starttransfer" > $SUBDIR/sessions.csv

SUBJ_COUNTER=0
SITE_COUNTER=0
for((study=1;study<=$STUDIES;study++))
do
	SITES=$(( (( RANDOM % $RAND_SITES ) + 1 )* (2)))
	if [ $SITES -gt 12 ]
	 then
		SITES=12
	 fi
	 if [ $SITES -lt 4 ]
	 then
		SITES=4
	 fi
	createSubjects
done

echo "$SUBJ_COUNTER SUBJ_COUNTER $TOTAL_SUBJECTS TOTAL_SUBJECTS"
while [ $SUBJ_COUNTER -lt $TOTAL_SUBJECTS ]; do
	SITES=200
	createSubjects
done
echo "Created $SUBJ_COUNTER subjects in $SITE_COUNTER projects"
