#/bin/sh
####################################################
## downloadAll.sh
## script for downloading all accessible image session data
##
## Designed to sync all available data to a project (potentially as a nightly cron job)
## If sessions are modified, the prior downloaded data will be moved to a recycling folder and redownloaded.
##
## Author: Tim Olsen <tim@radiologics.com>
####################################################

HOST="$1"
CRED="$2:$3"
DIR=`pwd`
OUTPUT="$4"
RECYCLE="$OUTPUT/recycle"
TIMEDIR=$(date "+%Y%m%d%H%M%S")
TEMP="$OUTPUT/temp-download"


if [ "$#" -ne 4 ]; then 
	echo "Inproper parameters"
	echo "Expected usage: downloadAll.sh URL USER PASS OUTPUT_DIR"
	echo "Example: downloadAllSessions.sh https://website.radiologics.com user password output"
	echo "HOST: url of server"
	echo "USER: username for host"
	echo "PASS: password for host"
	echo "OUTPUT_DIR: ./output"
	exit 1 
fi

die(){
  echo >&2 "$@"
  exit 1
}

echo "HOST: $HOST"
echo "OUTPUT: $OUTPUT"
echo "RECYCLE: $RECYCLE"
echo "TIMEDIR: $TIMEDIR"
echo "TEMP: $TEMP"

mkdir -p $TEMP

## first create a reusable JSESSIONID so we don't have to reauthenticate on every request.  This is optional but more efficient.
curl -k -X POST -u "$CRED" "$HOST/data/JSESSION" -i > ~/JSESSION.txt
JSESSION=`grep "JSESSIONID" ~/JSESSION.txt`
JSESSION=${JSESSION:23:32}
echo "JSESSION created: $JSESSION"
					
cd $DIR


#retrieve full list of experiments
curl -k -X GET -s -b "JSESSIONID=$JSESSION" "$HOST/data/experiments?format=csv&columns=ID,subject_ID,project,label,subject_label,last_modified&xnat:experimentData/meta/status=active,locked" > ./experiments.txt ||  die "failed to retrieve experiments"

sed -i "1d" experiments.txt

lines=`cat ./experiments.txt | wc -l`
COUNTER=0

echo "Experiments: $lines"

while read p; do
 exptId=`echo "$p" | cut -d',' -f1`
 subjId=`echo "$p" | cut -d',' -f2`
 exptLabel=`echo "$p" | cut -d',' -f6`
 subjLabel=`echo "$p" | cut -d',' -f3`
 proj=`echo "$p" | cut -d',' -f5`
 lastModified=`echo "$p" | cut -d',' -f7`
  
 writeTo="$OUTPUT/$proj/$subjLabel/$exptLabel"
 
 if [ -d "$writeTo" ]; then
	echo "$writeTo already exists"
	
	if [ -e "$writeTo/downloadDate.txt" ]; then
		DOWNLOADED=`cat $writeTo/downloadDate.txt`	
		dDOWNLOADED=`date -d "$DOWNLOADED" +"%F %T"`
		dlastModified=`date -d "$lastModified" +"%FT%T"`

		if [[ $dlastModified > $dDOWNLOADED ]]; then	
			echo 'Moving old data to recycling due to modification';
			echo "$writeTo -> $RECYCLE/$proj/$subjLabel/$exptLabel/$TIMEDIR"
			mkdir -p "$RECYCLE/$proj/$subjLabel/$exptLabel"
			mv $writeTo "$RECYCLE/$proj/$subjLabel/$exptLabel/$TIMEDIR"
			echo "re-downloading $writeTo..."
		else
			continue;
		fi
	else
		echo 'Incomplete download discovered...';
		echo 'Moving old data to recycling for re-download';
		echo "$writeTo -> $RECYCLE/$proj/$subjLabel/$exptLabel/$TIMEDIR"
		mkdir -p "$RECYCLE/$proj/$subjLabel/$exptLabel"
		mv $writeTo "$RECYCLE/$proj/$subjLabel/$exptLabel/$TIMEDIR"
		echo "re-downloading $writeTo..."
	fi
 else
	echo "downloading $writeTo..."
 fi
 	
 mkdir -p $writeTo
 
 if [ -d "$TEMP/download.${TIMEDIR}.zip" ]; then
 	rm $TEMP/download.${TIMEDIR}.zip
 fi
 
 curl -k -X GET -s -b "JSESSIONID=$JSESSION" "$HOST/data/experiments/${exptId}/files?format=zip&all=true" --output $TEMP/download.${TIMEDIR}.zip ||  die "failed to download experiment $exptLabel"
 
 echo "extracting $writeTo..."
 
 unzip -q -d "$OUTPUT/$proj/$subjLabel" $TEMP/download.${TIMEDIR}.zip  ||  die "failed to unzip downloaded data"
 
 rm $TEMP/download.${TIMEDIR}.zip || die "failed to clean up temp resources"
 
 echo $(date "+%F %T") > $writeTo/downloadDate.txt
 
 COUNTER=$((COUNTER + 1))
done <experiments.txt

echo "Closing JSESSION"
curl -k -X DELETE -b "JSESSIONID=$JSESSION" "$HOST/data/JSESSION"