#/bin/sh
####################################################
## deleteImageSessions.sh
## script for deletign all image session data
##
## Designed to delete all session data within a project
##
## Author: Tim Olsen <tim@radiologics.com>
####################################################

HOST="$1"
CRED="$2:$3"
PROJECT="$4"
TIMEDIR=$(date "+%Y%m%d%H%M%S")
TEMP="temp-expt-list-${TIMEDIR}.txt"
JSESFILE="./jsession-${TIMEDIR}.txt"

if [ "$#" -ne 4 ]; then 
	echo "Inproper parameters"
	echo "Expected usage: deleteImageSessions.sh URL USER PASS PROJECT"
	echo "Example: deleteImageSessions.sh https://website.radiologics.com user password PROJ_ID"
	echo "HOST: url of server"
	echo "USER: username for host"
	echo "PASS: password for host"
	echo "PROJECT: YOURPROJECT_ID"
	exit 1 
fi

die(){
  echo >&2 "$@"
  exit 1
}

echo "HOST: $HOST"
echo "TEMP: $TEMP"


## first create a reusable JSESSIONID so we don't have to reauthenticate on every request.  This is optional but more efficient.
curl -k -f -X POST -u "$CRED" "$HOST/data/JSESSION" -i > ${JSESFILE} ||  die "failed to create resusable JSESSION"
JSESSION=`grep "JSESSIONID" ${JSESFILE}`
JSESSION=${JSESSION:23:32} ||  die "failed to parse JSESSION"
if [ -z "$JSESSION" ]
then
	die "failed to parse JSESSION"
else
	echo "JSESSION created: $JSESSION"
fi
rm ${JSESFILE}
	
#retrieve full list of experiments
echo "curl -k -X GET -s -f -b JSESSIONID=$JSESSION $HOST/data/experiments?project=$PROJECT&format=csv&columns=ID,subject_ID,project,label,subject_label,last_modified > ./$TEMP"
curl -k -X GET -s -f -b "JSESSIONID=$JSESSION" "$HOST/data/experiments?project=$PROJECT&format=csv&columns=ID,subject_ID,project,label,subject_label,last_modified" > ./$TEMP ||  die "failed to retrieve experiments"
sed -i "1d" $TEMP

lines=`cat ./$TEMP | wc -l`
COUNTER=0

echo "Experiments: $lines"

while read p; do
 exptId=`echo "$p" | cut -d',' -f1`
 subjId=`echo "$p" | cut -d',' -f2`
 exptLabel=`echo "$p" | cut -d',' -f6`
 subjLabel=`echo "$p" | cut -d',' -f3`
 proj=`echo "$p" | cut -d',' -f5`
 lastModified=`echo "$p" | cut -d',' -f7`
  
 echo curl -k -X DELETE -f -b JSESSIONID=$JSESSION $HOST/data/projects/${PROJECT}/subjects/${subjLabel}/experiments/${exptLabel}?removeFiles=true ||  die failed to delete experiment $exptLabel
 curl -k -X DELETE -f -b "JSESSIONID=$JSESSION" "$HOST/data/projects/${PROJECT}/subjects/${subjLabel}/experiments/${exptLabel}?removeFiles=true" ||  die "failed to delete experiment $exptLabel"
  
 COUNTER=$((COUNTER + 1))
done <$TEMP

echo "Closing JSESSION"
curl -k -X DELETE -b "JSESSIONID=$JSESSION" "$HOST/data/JSESSION"