#/bin/sh
####################################################
## sendData1.sh
## script for uplaoding PET data (DICOM and NIFTI) of 2 different modalities.
## Tracer is identified based on the folder structure
##
## Author: Tim Olsen <tim@radiologics.com>
## Last Modified: 

HOST="https://demo12.radiologics.com"
CRED="$1:$2"
DIR=`pwd`



if [ "$#" -ne 2 ]; then 
	echo "Inproper parameters"
	echo "Expected usage: sendData1.sh USER PASS PROJECT_ID DIR"
	echo "USER: user login for https://SITEURL"
	echo "PASS: user password for https://SITEURL"
	exit 1 
fi

echo "Sending data in $DIR to $PROJ using account $1"

## first create a reusable JSESSIONID so we don't have to reauthenticate on every request.  This is optional but more efficient.
curl -X POST -u "$CRED" "$HOST/data/JSESSION" -i > ~/JSESSION.txt
JSESSION=`grep "JSESSIONID" ~/JSESSION.txt`
JSESSION=${JSESSION:23:32}
echo "JSESSION created: $JSESSION"
					
cd $DIR

curl -X GET -s -b "JSESSIONID=$JSESSION" "$HOST/data/prearchive?format=csv" > ./prearchive.txt ||  die "failed to upload files"

sed -i "1d" prearchive.txt

lines=`cat ./prearchive.txt | wc -l`
COUNTER=0

while read p; do
 subject=`echo "$p" | cut -d',' -f9`
 url=`echo "$p" | cut -d',' -f12`
 echo "URL:$url"
 echo "curl -b JSESSIONID=$JSESSION -X POST ${HOST}/data/services/archive?src=${url}&EXPT_LABEL=${subject}_${COUNTER}"
 
 curl -b "JSESSIONID=$JSESSION" -X POST "${HOST}/data/services/archive?src=${url}&EXPT_LABEL=${subject}_${COUNTER}"
 sleep 15
 COUNTER=$((COUNTER + 1))
done <prearchive.txt


echo "Closing JSESSION"
curl -X DELETE -b "JSESSIONID=$JSESSION" "$HOST/data/JSESSION"