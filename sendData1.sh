#/bin/sh
####################################################
## sendData1.sh
## script for uplaoding PET data (DICOM and NIFTI) of 2 different modalities.
## Tracer is identified based on the folder structure
##
## Author: Tim Olsen <tim@radiologics.com>
## Last Modified: 

HOST="https://SITEURL"
CRED="$1:$2"
PROJ=$3
cd $4
DIR=`pwd`



if [ "$#" -ne 4 ]; then 
	echo "Inproper parameters"
	echo "Expected usage: sendData1.sh USER PASS PROJECT_ID DIR"
	echo "USER: user login for https://SITEURL"
	echo "PASS: user password for https://SITEURL"
	echo "PROJECT_ID: project ID to upload data to (project should already exist, and user account needs to be an owner or member)"
	echo "DIR: directory which contains the data to upload.  Expected to contain folders like 'AD', 'NL', etc"
	exit 1 
fi

echo "Sending data in $DIR to $PROJ using account $1"

## first create a reusable JSESSIONID so we don't have to reauthenticate on every request.  This is optional but more efficient.
curl -X POST -u "$CRED" "$HOST/data/JSESSION" -i > ~/JSESSION.txt
JSESSION=`grep "JSESSIONID" ~/JSESSION.txt`
JSESSION=${JSESSION:23:32}
echo "JSESSION created: $JSESSION"
					
cd $DIR
for INITIALS in `ls .`
do
	for SUBJECT in `ls $INITIALS`
	do
	
		for TRACERDIR in `ls $INITIALS/$SUBJECT`
		do
			if [ "fdgPET" = $TRACERDIR ]; then
				TRACER="FDG"
			else
				TRACER="AV45"
			fi
			
			for VISIT in `ls $INITIALS/$SUBJECT/$TRACERDIR`
			do
				SESSION="${SUBJECT}_${VISIT}_${TRACER}"
			
				for SERIES in `ls $INITIALS/$SUBJECT/$TRACERDIR/$VISIT`
				do		
					cd $DIR
			
					echo "sending series $SERIES of $SESSION"	
										
					echo "creating subject"
					echo "curl -X PUT -b JSESSIONID=$JSESSION $HOST/data/projects/$PROJ/subjects/$SUBJECT?event_reason=batch upload"
					curl -X PUT -b "JSESSIONID=$JSESSION" "$HOST/data/projects/$PROJ/subjects/$SUBJECT?event_reason=batch upload"
					
					echo "creating session"
					echo "curl -X PUT -b JSESSIONID=$JSESSION $HOST/data/projects/$PROJ/subjects/$SUBJECT/experiments/$SESSION?xsiType=xnat:petSessionData&xnat:petSessionData/tracer_name=$TRACER&xnat:petSessionData/visit_id=$VISIT"
					curl -X PUT -b "JSESSIONID=$JSESSION" "$HOST/data/projects/$PROJ/subjects/$SUBJECT/experiments/$SESSION?xsiType=xnat:petSessionData&xnat:petSessionData/tracer_name=$TRACER&xnat:petSessionData/visit_id=$VISIT&event_reason=batch%20upload"

					echo "scan: $SERIES"
					curl -X PUT -b "JSESSIONID=$JSESSION" "$HOST/data/projects/$PROJ/subjects/$SUBJECT/experiments/$SESSION/scans/$SERIES?xsiType=xnat:petScanData&event_reason=batch%20upload"
					curl -X PUT -b "JSESSIONID=$JSESSION" "$HOST/data/projects/$PROJ/subjects/$SUBJECT/experiments/$SESSION/scans/$SERIES/resources/DICOM?event_reason=batch%20upload&event_action=Uploaded%20DICOM"
					
					SCANPATH="$INITIALS/$SUBJECT/$TRACERDIR/$VISIT/$SERIES"
					
					cd $SCANPATH/dcm
					
					echo "uploading DICOM"
					for DCM in `ls *.dcm`
					do
						echo "Uploading @$SCANPATH/dcm/$DCM"
						echo "DCM=$DCM"
						curl -X POST -b "JSESSIONID=$JSESSION" "$HOST/data/projects/$PROJ/subjects/$SUBJECT/experiments/$SESSION/scans/$SERIES/resources/DICOM/files/$DCM?inbody=true&update-stats=false&event_reason=batch%20upload" --data-binary "@$DCM"
					done				

					cd $DIR

					echo "pulling scan meta data from dicom"
					curl -X PUT -b "JSESSIONID=$JSESSION" "$HOST/data/projects/$PROJ/subjects/$SUBJECT/experiments/$SESSION/scans/$SERIES?xsiType=xnat:petScanData&pullDataFromHeaders=true&event_reason=batch upload"
					
					cd $SCANPATH/nii
					
					echo "upload NIFTI"
					##file names differ based on tracer
					if [ "fdgPET" = $TRACERDIR ]; then
						for NII in `ls f*`
						do
							curl -X POST -b "JSESSIONID=$JSESSION" "$HOST/data/projects/$PROJ/subjects/$SUBJECT/experiments/$SESSION/scans/$SERIES/resources/NII/files/$NII?inbody=true&update-stats=true&event_reason=batch%20upload&event_action=Uploaded%20NII" --data-binary "@$NII"
						done
						
						for NII in `ls w*`
						do
							curl -X POST -b "JSESSIONID=$JSESSION" "$HOST/data/projects/$PROJ/subjects/$SUBJECT/experiments/$SESSION/scans/$SERIES/resources/NORMALIZED/files/$NII?inbody=true&update-stats=true&event_reason=batch%20upload&event_action=Uploaded%20NORMALIZED NII" --data-binary "@$NII"
						done
					else
						for NII in `ls a*`
						do
							curl -X POST -b "JSESSIONID=$JSESSION" "$HOST/data/projects/$PROJ/subjects/$SUBJECT/experiments/$SESSION/scans/$SERIES/resources/NII/files/$NII?inbody=true&update-stats=true&event_reason=batch%20upload&event_action=Uploaded%20NII" --data-binary "@$NII"
						done
						
						for NII in `ls T1wa*`
						do
							curl -X POST -b "JSESSIONID=$JSESSION" "$HOST/data/projects/$PROJ/subjects/$SUBJECT/experiments/$SESSION/scans/$SERIES/resources/NORMALIZED/files/$NII?inbody=true&update-stats=true&event_reason=batch%20upload&event_action=Uploaded%20NORMALIZED%20NII" --data-binary "@$NII"
						done
					fi			

					cd $DIR
				done
				
				echo "populate scan types from DICOM"
				echo "this will review the series descriptions and map them to normalized scan types based on prior project precedent"
				curl -X POST -b "JSESSIONID=$JSESSION" "$HOST/data/projects/$PROJ/pipelines/triggerPipelines/experiments/$SESSION?suppressEmail=true&event_reason=populate scan types"
			done
		done
	done
done

echo "Closing JSESSION"
curl -X DELETE -b "JSESSIONID=$JSESSION" "$HOST/data/JSESSION"