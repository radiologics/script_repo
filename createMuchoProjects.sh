#/bin/sh

####################################################
## Author: Tim Olsen <tim@radiologics.com>
## Last Modified: 7/19/23
####################################################

HOST=$1
CRED=$2
STUDIES=$3
SUBJECTS=$4
RUN=$5

if [ "$#" -ne 5 ]; then 
	echo "Inproper parameters HOST CRED PROJECTS SUBJECTS RUN"
	exit 1 
fi

YEAR=1930

## first create a reusable JSESSIONID so we don't have to reauthenticate on every request.  This is optional but more efficient.
curl -k -X POST -u "$CRED" "$HOST/data/JSESSION" -i > ~/JSESSION.txt
JSESSION=`grep "JSESSIONID" ~/JSESSION.txt`
JSESSION=${JSESSION:23:32}
echo "JSESSION created: $JSESSION"

function createProject {
	SITE="P_${RUN}$study"
	PROJECT=$SITE
	PROJECT+="_"
	PROJECT+=$i
	
	 START=$(date +%s.%N)
	 echo "${RUN} Create project"
	curl  -b "JSESSIONID=$JSESSION" -X PUT $HOST/REST/projects/$PROJECT
	END=$(date +%s.%N)
	 DIFF=$(echo "$END - $START" | bc)
	 echo ".... ${DIFF}"

	for((sC=0;sC<$SUBJECTS;sC++))
	do
		SUBJECT="S$study"
		SUBJECT+="_"
		SUBJECT+=$i
		SUBJECT+="_"
		SUBJECT+=$sC
		

		if [ $YEAR -eq 1987 ]
		then
			YEAR=1940
		else
			YEAR=$((YEAR+1))
		fi
			
		 START=$(date +%s.%N)
		 echo "${RUN} Create subject"
		 curl --silent -w ',%{http_code},%{time_total},%{time_connect},%{time_appconnect},%{time_pretransfer},%{time_starttransfer}\n' -b "JSESSIONID=$JSESSION" -X PUT "$HOST/REST/projects/$PROJECT/subjects/$SUBJECT?event_reason=test&dob=02/14/$YEAR" >> $SUBDIR/subjects-curl.csv
		 END=$(date +%s.%N)
		 DIFF=$(echo "$END - $START" | bc)
		 echo ".... ${DIFF}"
		echo "${SUBJ_COUNTER},${DIFF}" >> $SUBDIR/subjects.csv

		 START=$(date +%s.%N)
		 echo "${RUN} Create PET session"
		 curl --silent -w ',%{http_code},%{time_total},%{time_connect},%{time_appconnect},%{time_pretransfer},%{time_starttransfer}\n'  -b "JSESSIONID=$JSESSION" -X PUT "$HOST/REST/projects/$PROJECT/subjects/$SUBJECT/experiments/MR_$SUBJECT?event_reason=test&req_format=form&xsiType=xnat:mrSessionData" -d "xnat:mrSessionData/date=01/11/2013" -d "xnat:mrSessionData/visit_id=v1" >> $SUBDIR/sessions-curl.csv
		 END=$(date +%s.%N)
		 DIFF=$(echo "$END - $START" | bc)
		 echo ".... ${DIFF}"
		
		 echo "${SUBJ_COUNTER},${DIFF}" >> $SUBDIR/sessions.csv
		 let SUBJ_COUNTER=SUBJ_COUNTER+1
	done
}

SUBDIR=$(date +%s)
mkdir $SUBDIR
echo "subject,total,connect,appconnect,pretransfer,starttransfer" > $SUBDIR/subjects.csv
echo "session,total,connect,appconnect,pretransfer,starttransfer" > $SUBDIR/sessions.csv

SUBJ_COUNTER=0
SITE_COUNTER=0
for((study=1;study<=$STUDIES;study++))
do
	createProject
done

echo "Created $SUBJ_COUNTER subjects in $STUDIES projects"
